﻿namespace MySqlMultiService
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnWEB = new System.Windows.Forms.Button();
            this.btnDESK = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // btnWEB
            // 
            this.btnWEB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWEB.Location = new System.Drawing.Point(74, 22);
            this.btnWEB.Name = "btnWEB";
            this.btnWEB.Size = new System.Drawing.Size(129, 27);
            this.btnWEB.TabIndex = 0;
            this.btnWEB.Text = "WEB Mysql";
            this.btnWEB.UseVisualStyleBackColor = true;
            this.btnWEB.Click += new System.EventHandler(this.btnWEB_Click);
            // 
            // btnDESK
            // 
            this.btnDESK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDESK.Location = new System.Drawing.Point(74, 55);
            this.btnDESK.Name = "btnDESK";
            this.btnDESK.Size = new System.Drawing.Size(129, 27);
            this.btnDESK.TabIndex = 1;
            this.btnDESK.Text = "DESK MySql";
            this.btnDESK.UseVisualStyleBackColor = true;
            this.btnDESK.Click += new System.EventHandler(this.btnDESK_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 500;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 96);
            this.Controls.Add(this.btnDESK);
            this.Controls.Add(this.btnWEB);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "MySql Services Switcher";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnWEB;
        private System.Windows.Forms.Button btnDESK;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
    }
}

