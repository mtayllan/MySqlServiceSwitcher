﻿using System;
using System.Drawing;
using System.ServiceProcess;
using System.Threading;
using System.Windows.Forms;

namespace MySqlMultiService
{
    public partial class Form1 : Form
    {

        private bool desk;
        private bool web;

        private ServiceController WEB_service;
        private ServiceController DESK_service;

        public Form1()
        {
            InitializeComponent();
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            btnWEB.BackColor = Color.Red;
            btnDESK.BackColor = Color.Red;

            if (WEB_service!=null && DESK_service != null)
            {
                if(WEB_service.Status == ServiceControllerStatus.Running)
                    btnWEB.BackColor = Color.Green;
                else if(DESK_service.Status == ServiceControllerStatus.Running)
                    btnDESK.BackColor = Color.Green;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ServiceController[] services = ServiceController.GetServices();
            foreach (ServiceController service in services)
            {
                if(service.DisplayName == "wampmysqld64")
                {
                    WEB_service = service;
                }
                else if (service.DisplayName == "MySQL57")
                {
                    DESK_service = service;
                }
            }
        }

      

        public void StartService(ServiceController service, int timeoutMilliseconds = 10000)
        {
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);
                service.Refresh();
                if (service.Status == ServiceControllerStatus.Stopped)
                {
                    service.Start();
                    service.WaitForStatus(ServiceControllerStatus.Running);
                    ticker(-1);
                }
                else
                {
                    throw new Exception(string.Format("{0} --> já esta iniciado.", service.DisplayName));
                }
            }
            catch
            {
                throw;
            }
        }

        public void StopService(ServiceController service, int timeoutMilliseconds = 10000)
        {            
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);
                service.Refresh();
                if (service.Status == ServiceControllerStatus.Running)
                {
                    service.Stop();
                    new Thread(() => {
                        ticker(service == WEB_service ? 1 : 0);
                        Console.Write("TESTE");
                    }).Start();                    
                    service.WaitForStatus(ServiceControllerStatus.Stopped);                    

                }
            }
            catch
            {
                throw;
            }
        }
        public void RestartService(string serviceName, int timeoutMilliseconds = 10000)
        {
            ServiceController service = new ServiceController(serviceName);
            try
            {
                int millisec1 = Environment.TickCount;
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);
                service.Refresh();
                if (service.Status != ServiceControllerStatus.Stopped)
                {
                    service.Stop();
                    service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
                    // conta o resto do timeout
                    int millisec2 = Environment.TickCount;
                    timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds - (millisec2 - millisec1));
                    service.Start();
                    service.WaitForStatus(ServiceControllerStatus.Running, timeout);
                }
                else
                {
                    service.Start();
                    throw new Exception(string.Format("{0} --> foi parado e a seguir iniciado", service.DisplayName));
                }
            }
            catch
            {
                throw;
            }
        }

        private void btnWEB_Click(object sender, EventArgs e)
        {
            StopService(DESK_service);
            StartService(WEB_service);
        }

        private void btnDESK_Click(object sender, EventArgs e)
        {
            StopService(WEB_service);
            StartService(DESK_service);
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            btnTicker.BackColor = btnTicker.BackColor == Color.Yellow ? Color.Orange : Color.Yellow;
        }

        Button btnTicker;

        private void ticker(int t)
        {
            if (t == 0)
            {
                btnTicker = btnWEB;
                timer2.Start();
            }
            else if (t == 1) 
            {
                btnTicker = btnDESK;
                timer2.Start();
            }else if (t == -1)
            {
                timer2.Stop();
            }

        }
    }
}
